package getterAndSetter;

public class getterAndSetter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Kode ini ditulis di dalam method main

		// membuat objek dari class User
		setterGetterClass dian = new setterGetterClass();

		// menggunakan method setter
		dian.setName("Alif");
		dian.setPassowrd("JavaIsTh3B3s");

		// menggunakan method getter
		System.out.println("Username: " + dian.getName());
		System.out.println("Password: " + dian.getPassowrd());

	}

}

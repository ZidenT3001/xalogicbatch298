package day2;

import java.util.Scanner;

public class Main {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Inputkan Hadiah Anda (1 - 5) :  ");
		int nomorInput = input.nextInt();
		//switch logic
		switch(nomorInput) {
			case 1:
				System.out.println("Selamat Anda Mendapat Mobil");
				break;
			case 2:
				System.out.println("Selamat Anda Mendapat Motor");
				break;
			case 3:
				System.out.println("Selamat Anda Mendapat Uang");
				break;
			case 4:
				System.out.println("selamat Anda Mendapat HP");
				break;
			case 5:
				System.out.println("Selamat Anda Mendapatkan Flash Disk");
				break;
			default:
				System.out.println("Nomor Yang Anda Inputkan Tidak Terdaftar");
				break;
		}
	}

}

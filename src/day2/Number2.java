package day2;

import java.util.Scanner;

public class Number2 {
public static void main (String[] args) {
	Scanner input = new Scanner(System.in);
	System.out.println("Total Belanjaan Anda Rp.- :  ");
	int totalBelanja = input.nextInt();
	System.out.println("Masukkan Kode Promo Jika Ada :  ");
	String kPromo = input.next().toUpperCase();
	input.nextLine();
	System.out.println("Input Jarak Order Anda :  ");
	int jarak = input.nextInt();
	double discount;
	double totalPembayaran;
	int ongkir = 5000;
	String kode = "JKTOVO";
	System.out.println("");
	if (kPromo.equals(kode) && totalBelanja >= 30000) {
		discount = (double) (0.4 * totalBelanja);
	}
	else {
		discount = 0;
	}
	if (jarak > 5) {
		ongkir = jarak * 1000;
	}
	else {
		ongkir = 5000;
	}
	
	totalPembayaran = totalBelanja - discount + ongkir;
	System.out.println("============================");
	System.out.println("Belanja : " + totalBelanja);
	System.out.println("Diskon 40% : " + discount);
	System.out.println("Ongkir : " + ongkir);
	System.out.println("Total Pembaran Anda : " + totalPembayaran);
	System.out.println("============================");	
}
}

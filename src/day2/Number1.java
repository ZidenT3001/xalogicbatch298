package day2;

import java.util.Scanner;

public class Number1 {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Inputkan Pulsa Yang Anda Mau :  ");
		int nomorInput = input.nextInt();
		//IF logic
		if (nomorInput >= 10000 && nomorInput < 25000) {
			System.out.println("Selamat Anda Mendapatkan Point 80");
		}
		else if (nomorInput >= 25000 && nomorInput <= 50000) {
			System.out.println("Selamat Anda Mendapatkan Point 200");
		}
		else if (nomorInput >= 50000 && nomorInput <= 100000) {
			System.out.println("Selamat Anda Mendapatkan Point 400");
		}
		else if (nomorInput > 100000) {
			System.out.println("Selamat Anda Mendapatkan Point 800");
		}
		else {
			System.out.println("Input Anda salah / Saldo yang anda mau tidak tersedia");
		}
	}

}

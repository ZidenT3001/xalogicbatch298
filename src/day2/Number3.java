package day2;

import java.util.Scanner;

public class Number3 {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Total Belanjaan Anda Rp.- :  ");
		int totalBelanja = input.nextInt();
		System.out.println("Masukkan Ongkir anda :  ");
		int ongkir = input.nextInt();
		input.nextLine();
		float discOngkir;
		float discBelanja;
		if (totalBelanja <= 30000) {
			discOngkir = 5000;
			discBelanja = 5000;
		}else if (totalBelanja >= 50000 && totalBelanja < 100000) {
			discOngkir = 10000;
			discBelanja = 10000;
		}else if (totalBelanja >= 100000) {
			discOngkir = 10000;
			discBelanja = 20000;
		}else {
			discOngkir = 0;
			discBelanja = 0;
		}
		float totalPembayaran;
		totalPembayaran = (totalBelanja - discBelanja) + (ongkir -discOngkir);
		System.out.println(totalBelanja);
		System.out.println("============================");
		System.out.println("Belanja : " + totalBelanja);
		System.out.println("Ongkos kirim : " + ongkir);
		System.out.println("Diskon Onkir  : " + discOngkir);
		System.out.println("Diskon Belanja  : " + discBelanja);
		System.out.println("Total Pembayaran  : " + totalPembayaran);
		System.out.println("============================");	
	}

}

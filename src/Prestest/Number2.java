package Prestest;

import java.util.Scanner;

public class Number2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan Kalimat Anda : ");
		String n = input.nextLine().toLowerCase();
		String ar = n.replaceAll(" ", "");
		char[] arr = ar.toCharArray();
		String vokal = " ";
		String consonan = " ";
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				if (arr[i] < arr[j]) {
					char temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		for ( int i = 0; i < arr.length; i++) {
			if ((arr[i] == 'a') || (arr[i] == 'i') || (arr[i] == 'u') || (arr[i] == 'e') || (arr[i] == 'o' )) {
				vokal += arr[i];
			}
			else {
				consonan += arr[i];
			}
		}
		System.out.print("huruf vokal " + vokal);
		System.out.println(" ");
		System.out.print("huruf Konsonan " + consonan);
		
		
	}
	
}

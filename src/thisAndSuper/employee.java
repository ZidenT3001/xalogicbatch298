package thisAndSuper;

public class employee extends person{
	
	float salary = 4000f;
	String name = "Dian";
	int age = 23;

	public void showInfo() {
		System.out.println("Name: " + super.name);
		System.out.println("Age: " + super.age);
		System.out.println("Salary: $" + salary);
	}
}

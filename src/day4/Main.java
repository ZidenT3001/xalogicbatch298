package day4;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("inputkan panjang array ada : ");	
		int n = input.nextInt();
		int[] arrayAngka = new int[5];
		String dua = "2";
		// conversi array ke integer
		int tempAngka = Integer.parseInt(dua);
		// conversi integer ke string 
		// angka[2] = 10000;
		// array[2] = array + ""; //convers		
		//input nilai ke dalam array
		arrayAngka[0] = 1;
		arrayAngka[1] = tempAngka;
		arrayAngka[2] = 3;
		arrayAngka[3] = 4;
		arrayAngka[4] = 5;
		// print array indeks ke -
		for (int i = 0; i < arrayAngka.length; i++) {
			System.out.print(arrayAngka[i] + " ");
		}
		System.out.println("");	
		System.out.println("====================");			
		int value = 1;
		int[] angka = new int[n];
		for (int i = 0; i < angka.length; i++) {
			angka[i] = value;
			value+=1;
		}
		for (int i = 0; i < angka.length; i++) {
			System.out.print(angka[i] + " ");
		}
		input.close();
		
	}
}

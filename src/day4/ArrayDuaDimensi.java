package day4;

import java.util.Scanner;

public class ArrayDuaDimensi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// arrayDuaDimensi[2][2] = 13;
		Scanner input = new Scanner(System.in);
		System.out.print("inputkan Jumlah Baris anda : ");
		int baris = input.nextInt();
		System.out.print("inputkan Jumlah Kolom anda : ");
		int kolom = input.nextInt();
		int value = 1;
		int[][] arrayDuaDimensi = new int[baris][kolom];
		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {			
				arrayDuaDimensi[i][j] = value;
				System.out.print(arrayDuaDimensi[i][j] + "     ");
				value += 1;
			}
			System.out.println(" ");
			
		}
		input.close();
		

	}

}

package day1;

import java.util.Scanner;

public class Percabangan {
	public static void main (String[] args) {
		System.out.println("=====================================");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Nama anda = ");
		String nama = input.nextLine(); //Input Nama
		System.out.println("Masukkan Nilai Anda = ");		
		int nilai = input.nextInt(); //Input nilai
		System.out.println("Masukkan Nilai SoftSkill Anda = ");
		int softSkill = input.nextInt(); //Input SoftSkill
		//Jika ada perpidahan tipe data atau berbeda type data pakai input.nextLine()
		input.nextLine();
		System.out.println("Masukkan Nomor Bacth anda = ");
		String batch = input.nextLine();
		String grade = "";
		String kelulusan = "";
		/*
		Nilai >= 90 = A , Lulus
		Nilai >= 85 = B+ , Lulus
		Nilai >= 80 = B , Lulus
		Nilai >= 75 = C+ , Lulus
		Nilai >= 70 = C , Remedial
		Nilai <> 70 = D , Tidak Lulus */
				
		if (nilai >= 90 && softSkill > 5) {
			grade = "A";
			kelulusan = "Lulus";
		}else if (nilai >= 85 && nilai <= 90 && softSkill > 5) {
			grade = "B+";
			kelulusan = "Lulus";
		}else if (nilai >= 80 && nilai <= 85 && softSkill > 5) {
			grade = "B";
			kelulusan = "Lulus";
		}else if (nilai >= 75 && nilai <= 80 && softSkill > 5) {
			grade = "C+";
			kelulusan = "Lulus";
		}else if (nilai >= 70 && nilai <= 75 && softSkill > 5) {
			grade = "C";
			kelulusan = "Remedial";
		}else{
			grade = "D";
			kelulusan = "Tidak Lulus";
		}
		System.out.println("====== " + batch + " ========= ");
		System.out.println("Grade Anda : " + grade);
		System.out.println("Kelulusan Anda : " + kelulusan);
		System.out.println("============================== ");
		
	}
}

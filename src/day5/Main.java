package day5;

import java.util.Scanner;

public class Main {
	
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter Nomor yang anda : ");
				int number = input.nextInt();
				input.nextLine();
				switch (number) {
					case 1:
						soal1();
						break;
					case 2:
						soal2();
						break;
					case 3:
						soal3();
						break;
					case 4:
						soal4();
						break;
					case 5:
						soal5();
						break;
					case 6:
						soal6();
						break;
					default :
						System.out.println("Input Anda Salah !");
						break;
				
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println();
			System.out.println("continue?");
			answer = input.next();	
		
	}

}
	
	private static void soal1() {
		System.out.println("Inputkan Uang Anda : " );
		int uang = input.nextInt();
		input.nextLine();
		System.out.println("Inputkan Harga Baju : " );
		String baju = input.nextLine();			
		System.out.println("Inputkan Harga Baju : " );
		String celana = input.nextLine();
		String[] tempBaju = baju.split(",");
		String[] tempCelana = celana.split(",");
		int hargaSetPakaian = 0;
		int beliSetelan = 0; // 75,
		for (int i = 0; i < tempBaju.length; i++) {			
				hargaSetPakaian = Integer.parseInt(tempBaju[i]) + Integer.parseInt(tempCelana[i]); //75, 70, 90
				if (hargaSetPakaian <= uang && hargaSetPakaian >= beliSetelan ) {
					beliSetelan = hargaSetPakaian;
				}				
		}
		System.out.println("Harga Set Pakaian yang Anda Beli : " + beliSetelan);
			
	}
	
	private static void soal2() {
		System.out.print("Inputkan array anda : ");
		String inputan = input.nextLine().toLowerCase();
		System.out.print("Inputkan Jumlah Rotation anda : ");
		int rotation = input.nextInt();
		String[] arrayInputan = inputan.split(",");
		for (int i = 0; i < arrayInputan.length; i++) {
			System.out.print(arrayInputan[i]);
		}
		for (int i = 0; i < rotation; i++) {
			String first = arrayInputan[0];
			for (int j = 0; j < arrayInputan.length-1; j++) {
				arrayInputan[j] = arrayInputan[j+1];
			}
			arrayInputan[arrayInputan.length-1]=first;
		}
		System.out.println("");
		System.out.println("Out Put Array : ");
		for ( int k = 0; k < arrayInputan.length; k++) {
			System.out.print(arrayInputan[k]);
		}
		
				
	}
	private static void soal3() {
		System.out.println("Inputkan jumlah Puntung Rokok Yang Terkoumpul Oleh Pemulung : ");
		int n = input.nextInt();
		int batangRokok = n / 8;
		int sisa = n % 8;
		int uang = batangRokok * 500;
		System.out.println("Banyak batang Rokok : " + batangRokok);
		System.out.println("sisa batang rokok : " + sisa);
		System.out.println("uang yang peroleh oleh pemulung : " + "Rp. " + uang);
		
		
		
	}
	private static void soal4() {
		System.out.print("Inputkan Total Menu : ");
		int menu = input.nextInt();
		input.nextLine();
		System.out.print("Makana Yang Memiliki Alergi Index ke - : ");
		int alergi = input.nextInt();
		input.nextLine();
		System.out.print("Inputkan Harga Makana : ");
		String harga = input.nextLine();
		System.out.print("Inputkan Uang Elsa  : ");
		int elsa = input.nextInt();
		String[] arrayHarga = harga.split(",");
		System.out.println(arrayHarga[1]);
		int totalHarga = 0;
		int hargaElsa = 0;
		int hargaYangElsaBayar = 0;
		int sisaUang = 0;
		for (int i = 0; i < arrayHarga.length; i++) {
			if (i != alergi) {
				hargaElsa = hargaElsa + Integer.parseInt(arrayHarga[i]);	
			}					
		}
		for (int i = 0; i < arrayHarga.length; i++) {
			totalHarga = totalHarga + Integer.parseInt(arrayHarga[i]);						
		}
		
		System.out.println("Toal Harga Yang Di bayar keduanya : " + totalHarga);
		System.out.println("Harga Makan yang Bisa Dimakan Oleh Elsea : " + hargaElsa);
		hargaYangElsaBayar = hargaElsa / 2;
		System.out.println("Harga Yang Harus Dibaya Elsa : " + hargaYangElsaBayar);
		sisaUang = elsa - hargaYangElsaBayar;
		System.out.println("Sisa Uang Elsa  : " + sisaUang);
	}
	private static void soal5() {
		System.out.print("Inputkan kalimat anda : ");
		String inputan = input.nextLine().toLowerCase();
		String charInputan = inputan.replaceAll(" ", "");
		char[] arrayInputan = charInputan.toCharArray();		
		char vokal, konsonan;
		for (int a = 0; a < arrayInputan.length; a++) { // 0
			for (int b = 0; b < arrayInputan.length; b++) {  // 0,1
				if (arrayInputan[a] < arrayInputan[b]) {
					char temp = arrayInputan[a];
					arrayInputan[a] = arrayInputan[b];
					arrayInputan[b]= temp;
					
				}
			}
		}
		for (int c = 0; c < arrayInputan.length; c++) {
			System.out.print(arrayInputan[c]);
		}
		int hitungan = 0;
		System.out.println();
		System.out.print("huruf Vokal Adalah : ");
		for (int i = 0; i < arrayInputan.length; i++) {
			if (arrayInputan[i] =='a' || arrayInputan[i] == 'i' || arrayInputan[i] == 'u' || arrayInputan[i] == 'e' || arrayInputan[i] == 'o') {
					vokal = arrayInputan[i];
					System.out.print(vokal);
					hitungan = hitungan + 1;
			}
		}
		System.out.println("");
		System.out.print("huruf Konsonan Adalah : ");
		for (int i = 0; i < arrayInputan.length; i++) {
			if (arrayInputan[i] =='a' || arrayInputan[i] == 'i' || arrayInputan[i] == 'u' || arrayInputan[i] == 'e' || arrayInputan[i] == 'o') {
				
			}
			else {
				konsonan = arrayInputan[i];
				System.out.print(konsonan);
			}
		}
		System.out.println("");
		System.out.println("Jumlah Huruf Vokal Yang Ada = " + hitungan);
		
	}
	private static void soal6() {
		System.out.print("Inputkan kalimat anda : ");
		String inputan = input.nextLine().toLowerCase();
		String charInputan = inputan.replaceAll(" ", "");
		char[] arrayInputan = charInputan.toCharArray();
		for (int i = 0; i < arrayInputan.length; i++) {
			if (i % 2 != 0) {
				System.out.print(arrayInputan[i] = '*'); 				
			}else {
				System.out.print(arrayInputan[i]); 
			}
		}
		
	}
	
	
}

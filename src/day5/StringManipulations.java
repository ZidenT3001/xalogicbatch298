package day5;

public class StringManipulations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// String manipulations
				String name = "Alif Ziden Tantowi";
				
				//memisahkan kalimat menjadi perkata ( menggunakan split)
				String[] tempNama = name.split(" ");
				System.out.println("Nama Anda : " + tempNama[tempNama.length - 1]);
				
				//mengubah kata menjadi karakter 
				char[] charNama = tempNama[1].toCharArray();
				System.out.println(charNama[charNama.length - 1]);
				
				//mengganti karakter dari kalimat
				String replaceNama = name.replaceAll(" ", "");
				System.out.println(replaceNama);
				
				//memecah nama menjadi karakter
				char[] charReplaceNama = replaceNama.toCharArray();
				System.out.println(charReplaceNama[7]);
				
				String Ziden = name.substring(5, 13);
				System.out.println(Ziden);
				
				String lowerNama = name.toLowerCase().replaceAll(" ", "");
				
				char[] tempLowerNama = lowerNama.toCharArray();
				System.out.println(lowerNama);
				
				String upperNama = name.toUpperCase();
				System.out.println(upperNama);
	}

}

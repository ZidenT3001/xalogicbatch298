package day9;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				String answer = "Y";
				while (answer.toUpperCase().equals("Y")) {
					try {
						System.out.print("Enter Nomor yang anda : ");
						int number = input.nextInt();
						input.nextLine();
						switch (number) {
							case 1:
								soal1();
								break;
							case 2:
								soal2();
								break;
							case 3:
								soal3();
								break;
							case 4:
								soal4();
								break;
							case 5:
								soal5();
								break;
							case 6:
								soal6();
								break;
							case 7:
								soal7();
								break;
							case 8:
								soal8();
								break;
							default :
								System.out.println("Input Anda Salah !");
								break;
						
						}
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
					
					System.out.println();
					System.out.println("continue?");
					answer = input.next();
				}

		}

		private static void soal1() {
			String answer = "Y";
			int point = 15;
			while (answer.toUpperCase().equals("Y")) {
				try {
					System.out.print("Inputkan Jawaban Anda : ");
					String n = input.nextLine().toUpperCase();
					input.nextLine();
					System.out.print("Inputkan Taruhan Anda : ");
					int bet = input.nextInt();
					int randomNum = (int)(Math.random() * 11);  // 0 to 100
					System.out.println(randomNum);
					int result = 1;
					if (randomNum < 5 && n == "D") {
						point = point + bet;
						System.out.println(randomNum);
						System.out.println("You Win");
						System.out.println("Point saat ini : " + point);
					}
					else if (randomNum > point && n == "U") {
						point = point + bet;
						System.out.println(randomNum);
						System.out.println("You Win");
						System.out.println("Point saat ini : " + point);
					}
					else if (randomNum == 5 && n == "U") {
						point = point;
						System.out.println(randomNum);
						System.out.println("Draw !");
						System.out.println("Point saat ini : " + point);
					}
					else {
						point = point - bet;
						System.out.println(randomNum);
						System.out.println("Loser !");
						System.out.println("Point saat ini : " + point);
					}
				}
				catch (Exception e) {
				System.out.println(e.getMessage());
				}
				System.out.println();
				System.out.println("Lanjut Judi?");
				answer = input.next();
			}
		}


		private static void soal2() {
			System.out.println("Ada berpa buah di keranjang 1 : ");
			String keranjang1 = input.nextLine();
			System.out.println("Ada berpa buah di keranjang 2 : ");
			String keranjang2 = input.nextLine();
			System.out.println("Ada berpa buah di keranjang 1 : ");
			String keranjang3 = input.nextLine();
			System.out.println("Keranjang Mana Yang di Bawa Ibu ke pasar : ");
			int keranjang = input.nextInt();
			int result = 0;
			if (keranjang1.equals("KOSONG")) {
				keranjang1 = "0";
			}
			if (keranjang2.equals("KOSONG")) {
				keranjang2 = "0";
			}
			if (keranjang3.equals("KOSONG")) {
				keranjang3 = "0";
			}
			switch(keranjang) {
			  case 1:
			    result = Integer.parseInt(keranjang2) + Integer.parseInt(keranjang3);
			    System.out.println("Sisa Buah Di Rumah : " + result);
			    break;
			  case 2:
				result = Integer.parseInt(keranjang3) + Integer.parseInt(keranjang1);
				System.out.println("Sisa Buah Di Rumah : " + result);
			    break;
			  case 3:
				result = Integer.parseInt(keranjang2) + Integer.parseInt(keranjang1);
				System.out.println("Sisa Buah Di Rumah : " + result);
				break;
			  default:
			    System.out.println("Ibu Tidak Pergi Kepasar atau Ibu Kepasa Tanpa Bawa Keranjang hehe");
			} 
		}

		private static void soal3() {
			System.out.print("inputkan Jumlah Anak Yang Duduk : ");
			int n = input.nextInt();
			int result = 1;
			for (int i = n; i > 0; i--) {
				result = result * i;		
			}
			System.out.println("Banyak Susunan Merek Duduk Bersam Adadalh " + result + " Cara");

		}
		private static void soal4() {
			System.out.print("Jumlah Laki-Laki Dewasa : ");
			int a = input.nextInt();
			System.out.print("Jumlah Wanita Dewasa : ");
			int b = input.nextInt();
			System.out.print("Jumlah Anak-Anak : ");
			int c = input.nextInt();
			System.out.print("Jumlah bayi : ");
			int d = input.nextInt();
			int result = (a * 1) + (b * 2) + (c * 3) + (d * 5);
			int result2 = (a * 1) + (b * 3) + (c * 3) + (d * 5);
			if (result % 2 != 0 && result > 10) {
				System.out.println("Total Baju : " + result2);
			}
			else {		
				System.out.println("Total Baju : " + result);
			}
		}
		private static void soal5() {
			System.out.println("Berapa Kue yang akan anda buat ? : ");
			int n = input.nextInt();
			double terigu = 8.333333333333334;
			double gulaPasir = 6.666666666666667;
			double susu = 6.666666666666667;
			double telur = 6.666666666666667;
			terigu = terigu * n;
			gulaPasir = gulaPasir * n;
			susu = susu * n;
			telur = telur * n;
			System.out.println("untuk membuat " + n + " Kue Pukis di Butuhkan : ");
			System.out.println("1. Terigu : " + (int)terigu + " Gram");
			System.out.println("1. Gula Pasir  : " + (int)gulaPasir + " Gram");
			System.out.println("1. Susu Murni : " + (int)susu + " Gram");
			System.out.println("1. Putih Telur : " +(int) telur + " Gram");
		}

		private static void soal7() {
		    System.out.println("Tanggal Peminjaman : ");
		    String stglAwal = input.nextLine();
		    System.out.println("Tanggal Peminjaman : ");
		    String stglAkhir = input.nextLine();
		    DateFormat dateAwal = new SimpleDateFormat("dd/MM/yyyy");
		    DateFormat dateAkhir = new SimpleDateFormat("dd/MM/yyyy");
		    try {
		        Date tglAwal = dateAwal.parse(stglAwal);
		        Date tglAkhir = dateAkhir.parse(stglAkhir);
		        Date TGLAwal = tglAwal;
		        Date TGLAkhir = tglAkhir;
		        Calendar cal1 = Calendar.getInstance();
		        cal1.setTime(TGLAwal);
		        Calendar cal2 = Calendar.getInstance();
		        cal2.setTime(TGLAkhir);
		        
		        String hasil = String.valueOf(daysBetween(cal1, cal2));
		        long denda = (Integer.parseInt(hasil) - 3)*500;
		        System.out.println("Tanggal Awal  = " +stglAwal);
		        System.out.println("Tanggal Akhir = " +stglAkhir);
		        System.out.println("Selisih: " +hasil+ " hari");
		        System.out.println("Denda: Rp." +denda);
		    } catch (ParseException e) {
		    }
		 }

		private static void soal6() {
		    System.out.println("Tanggal Peminjaman : ");
		    String stglAwal = input.nextLine();
		    System.out.println("Tanggal Pengembalian : ");
		    String stglAkhir = input.nextLine();
		    DateFormat dateAwal = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		    DateFormat dateAkhir = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		    try {
		        Date tglAwal = dateAwal.parse(stglAwal);
		        Date tglAkhir = dateAkhir.parse(stglAkhir);
		        Date TGLAwal = tglAwal;
		        Date TGLAkhir = tglAkhir;
		        Calendar cal1 = Calendar.getInstance();
		        cal1.setTime(TGLAwal);
		        Calendar cal2 = Calendar.getInstance();
		        cal2.setTime(TGLAkhir);
		        
		        String hasil = String.valueOf(jam(cal1, cal2));        	
		        long denda = (Integer.parseInt(hasil)*3000);
		        System.out.println("Tanggal Awal  = " +stglAwal);
		        System.out.println("Tanggal Akhir = " +stglAkhir);
		        System.out.println("Selisih: " +hasil+ " menit");
		        System.out.println("Denda: Rp." +denda);
		    } catch (ParseException e) {
		    }
			 
		}
		private static void soal8() {
			System.out.print("Inputkan Kalimat Anda : ");
			String n = input.nextLine();
			char[] arr = n.toCharArray();
			boolean result = false;
			int batas = arr.length / 2;
			int a = 1;
			for (int i = 0; i < batas; i++) {
				if (arr[i] == arr[arr.length-a]) {
					result = true;
				}
				else {
					result = false;
					break;
				}
				a++;		
			}
			if (result == true) {
				System.out.println("YES");
			}
			else if (n.equals("bakso") || n.equals("Sop")) {
				System.out.println("Tumpah Dong !!");
			}
			else {
				System.out.println("NO");
			}
		}
		private static long daysBetween(Calendar tanggalAwal, Calendar tanggalAkhir) {
		    long lama = 0;
		    Calendar tanggal = (Calendar) tanggalAwal.clone();
		    while (tanggal.before(tanggalAkhir)) {
		        tanggal.add(Calendar.DAY_OF_MONTH, 1);
		        lama++;
		    }
		    return lama;
		}

		private static long jam(Calendar tanggalAwal, Calendar tanggalAkhir) {
		    long menit =0;
		    Calendar tanggal = (Calendar) tanggalAwal.clone();
		    Calendar tanggal2 = (Calendar) tanggalAwal.clone();
		    Calendar tanggal3 = (Calendar) tanggalAwal.clone();
		    while (tanggal.before(tanggalAkhir)) {
		    	tanggal.add(Calendar.MINUTE, 1);
		        menit++;
		    }
		    return menit;

}
}

package day10;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter Nomor yang anda : ");
				int number = input.nextInt();
				input.nextLine();
				switch (number) {
					case 1:
						soal1();
						break;
					case 2:
						soal2();
						break;
					case 3:
						soal3();
						break;
					case 4:
						soal4();
						break;
					case 5:
						soal5();
						break;
					case 6:
						soal6();
						break;
					case 7:
						soal7();
						break;
					case 8:
						soal8();
						break;
					default :
						System.out.println("Input Anda Salah !");
						break;
				
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println();
			System.out.println("continue?");
			answer = input.next();
		}

}

private static void soal1() {	
	System.out.println("Inputkan panjang array : ");
	Integer n = input.nextInt();
	String[] arr = new String[n];	
	for (int i = 0; i < n; i++) {
		input.nextLine();
		System.out.print("value ke- " + i + " : ");
		String val = input.nextLine();
		arr[i] = val;		
	}
	System.out.println("Sample Input");
	BigInteger[] series = new BigInteger[n];
	for(int i=0; i<n; i++){
	    series[i] = new BigInteger(arr[i]);
	    System.out.println(series[i]);
	}
	for (int j = 0; j < n; j++) {
		for (int k = 0; k < n; k++) {
			if (series[j].compareTo(series[k])== -1) {
				BigInteger temp = series[j];
				series[j] = series[k];
				series[k] = temp;
			}
		}
	}
	System.out.println("Sample output");
	for (int x = 0; x < n; x++) {
		System.out.println(series[x]);
	}
}
private static void soal2() {
	System.out.println("Input Your Array Length : ");
	int n = input.nextInt();
	input.nextLine();
	System.out.println("Input Yout Array Value : ");
	String value = input.nextLine();
	String[] str = value.split(" ");
	String last = str[str.length-1];
	String num = "";	
	for (int i = 1; i < n; i++) {
		int j = i - 1;
		num = str[i];		
		while ((j >= 0) && Integer.parseInt(str[j])>Integer.parseInt(num)){
			str[j + 1] = str[j];
			j--;
			for (int k = 0; k < n; k++) {
				System.out.print(str[k]+" ");
			}
			System.out.println("");
		}
		str[j + 1] =num;
	}
	for (int i = 0; i < n; i++) {
		System.out.print(str[i] + " ");
	}
}

private static void soal3() {
	System.out.print("Input Your Array Length :");
	int n = input.nextInt();
	input.nextLine();
	System.out.print("Input Your Array Value ");
	String a = input.nextLine();
	String[] f = a.split(" ");
	for (int j = 0; j < n; j++) {
		for (int k = 0; k < n; k++) {
			if (Integer.parseInt(f[j]) < Integer.parseInt(f[k])) { //[7 < 7, 7 < 4,]
				String temp = f[j];
				f[j] = f[k];
				f[k] = temp;
			}
		}
	}
	for (int i = 0; i < n; i++) {
		System.out.print(f[i] + " ");
	}
}
private static void soal4() {
	System.out.println("Input Your Array Length : ");
	int n = input.nextInt();
	input.nextLine();
	System.out.println("Input Yout Array Value : ");
	String value = input.nextLine();
	String[] str = value.split(" ");
	String num = "";
	int count = 0;
	System.out.println("Iteration || array ||shift");
    for (int j = 1; j < n; j++) {  
        String key = str[j];  
        int i = j-1;  
        while ( (i > -1) && ( Integer.parseInt(str[i]) > Integer.parseInt(key) ) ) {  
            str [i+1] = str [i];
            count++;
            i--;
            for (int k = 0; k < n; k++) {
				System.out.print(str[k] + " ");
			}
			System.out.println("");
            
        }  
        str[i+1] = key;  
    }
	for (int i = 0; i < n; i++) {
		System.out.print(str[i] + " ");
	}
	System.out.println("");
	System.out.println("total =  " + count);
}
private static void soal5() {
	try{
        BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Inputkan Panjang Array Anda : ");
        int n=Integer.parseInt(buf.readLine());
        int[] count=new int[100];
        System.out.println("Inputkan Array Anda : ");
        String[] str=buf.readLine().split(" ");
        for(int i=0;i<n;i++){
            count[Integer.parseInt(str[i])]++;
        }
        for(int i=0;i<99;i++){
            System.out.print(count[i]+" ");
        }
        System.out.println(count[99]);
    }catch(Exception e){}
}

private static void soal6() {
	try{
        BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Inputkan Panjang Array Anda : ");
        int n=Integer.parseInt(buf.readLine());
        int[] count=new int[100];
        System.out.println("Inputkan Arrya Anad : ");
        String[] str=buf.readLine().split(" ");
        String num = "";
    	for (int i = 1; i < n; i++) {
    		int j = i - 1;
    		num = str[i];		
    		while ((j >= 0) && Integer.parseInt(str[j])>Integer.parseInt(num)){
    			str[j + 1] = str[j];			
    			j--;
    		}
    		str[j + 1] =num;
    	}	
    	for (int i = 0; i < n; i++) {
    		System.out.print(str[i] + " ");
    	}
    }catch(Exception e){}
}

private static void soal7() {
	System.out.print("Input Your Array Length :");
	int n = input.nextInt();
	input.nextLine();
	System.out.print("Input Your Array Value ");
	String a = input.nextLine();
	String[] f = a.split(" ");
	for (int j = 0; j < n; j++) {
		for (int k = 0; k < n; k++) {
			if (Integer.parseInt(f[j]) < Integer.parseInt(f[k])) { //[7 < 7, 7 < 4,]
				String temp = f[j];
				f[j] = f[k];
				f[k] = temp;
			}
		}
	}
	for (int i = 0; i < n; i++) {
		System.out.print(f[i] + " ");
	}
	System.out.println("");
	if (n % 2 != 0 ) {
		int median = (n / 2);
		System.out.println(median);
		System.out.println("Median Dari Array Yang Di Inputkan : " + f[median]);
	}
	else {
		int x = (n / 2) - 1;		
		double y = (Integer.parseInt(f[x]) + Integer.parseInt(f[x + 1]));
		double median = y / 2;
//		System.out.println(x);
//		System.out.println(y);
//		System.out.println(median);
		System.out.println("Median Dari Array Yang Di Inputkan : " + median);
	}
	
}
private static void soal8() {
	System.out.print("Input Your Array Value ");
	String a = input.nextLine();
	char[] f = a.toCharArray();
//	for (int i = 0; i < f.length; i++) {
//		int ascii = (int) f[i];
//	}
	for (int j = 0; j < f.length; j++) {
		for (int k = 0; k < f.length; k++) {
			if ((f[j]) < (f[k])) { 
				char temp = f[j];
				f[j] = f[k];
				f[k] = temp;
			}
		}
	}
	for (int i = 0; i < f.length; i++) {
		System.out.print(f[i]);
	}
}

}

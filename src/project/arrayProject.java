package project;

import java.util.Scanner;

public class arrayProject {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("inputkan jumlah baris :");
		int baris = input.nextInt();
		System.out.print("inputkan jumlah kolom :");
		int kolom = input.nextInt();
		int[][] arr = new int[baris][kolom];
		for (int i= 0; i < baris; i++) {
			for (int j = 0; j < kolom; j++) {
				System.out.print("Inputkan nilai barism ke-" + i + " kolom ke-" + j + " :");
				int value = input.nextInt();
				arr[i][j] = value;
			}
		}
		for (int i= 0; i < baris; i++) {
			int sum = 0;
			for (int j = 0; j < kolom; j++) {
				sum += arr[i][j];
			}
			System.out.println("Hasil Penjumlahan Baris ke-" + i + " " + sum);
		}
		System.out.println(" ");
		
		for (int i = 0; i < baris; i++) {
			int max = 0;
			for (int j = 0; j < kolom; j++) {
				if (arr[i][j] > max) {
					max = arr[i][j];
				}
			}
			System.out.println("Nilai MAX Baris ke-" + i + " " + max);
		}

	}

}

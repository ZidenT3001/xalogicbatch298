package project;

import java.util.Scanner;

public class Pattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan Jumlah COLUMN : ");
		int n = input.nextInt();		
		for (int i =0 ; i < n; i++) {
			int num = 1;
			for (int j = 0; j < n; j++) {
				if ( j <= i ) {
					System.out.print(num);
					num++;
				}
			}
			System.out.println("");
		}
		System.out.println(" ");		
		for (int i = 0; i < n; i++) {
			int num1 = 1;
			for (int j = 0 ; j < n; j++) {
				if (i + j == n -1 || i + j > n - 1) {
					System.out.print(num1);
					num1++;
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println(" ");
		}		
		System.out.println(" ");
		for (int i = 0; i < n; i++) {
			int num2 = 1;
			for ( int j = 0; j < n; j++) {				
				if (i + j == n-1 || i + j < n-1) {
					System.out.print(num2);
					num2++;
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}
		System.out.println(" ");
		for (int i =0; i < n; i++) {
			int num3 = 1;
			for (int j = 0; j < n; j++) {
				if ( i == j || i < j) {
					System.out.print(num3);
					num3++;
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}
		System.out.println(" ");
	}

}

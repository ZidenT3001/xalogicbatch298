package project;

import java.util.Scanner;

public class gunung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Scanner in = new Scanner(System.in);
        System.out.print("panjang route : ");
        int n = in.nextInt();
        System.out.print("route : ");
        String s = in.next();
        
        int result = countingValleys(n, s);
        int result1 = countingValleys(n, s);
       
        System.out.println("Jumlah Lembah : " + result);
        in.close();

	}
	
	    static int countingValleys(int n, String s) {
	        // initialize variabel
	        int valley = 0;
	        int level = 0;
	        // kita gunakan looping untuk mengecek level permukaan
	        for(char current:s.toCharArray()){
	            // jika U maka level tambah + 1
	            if(current == 'U') ++level;
	            // jika D maka level tambah - 1
	            if(current == 'D') --level;
	            
	            // kita berfokus pada level permukaan 
	            // karena setiap lembah pasti akan diakhiri dengan
	            // level permukaan air laut dan Langkah 'Naik'
	            if(level == 0 && current == 'U'){
	                valley++;  // jika benar maka valley tambah.
	            }
	        }
	        
	        return valley;
	    }
	

}

package project;

import java.util.Scanner;

public class rutoToko {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] jarak = { 2000, 500, 1500, 2500 };
		// 1 liter untuk 2500
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan rute: ");
		String rute = input.nextLine().toLowerCase();// "Toko - Tempat 1 - Tempat 2 - Toko"
		String[] arrayRute = rute.split(" - ");

		int tempTotalJarak = 0;
		for (int i = 0; i < arrayRute.length; i++) {
			if (i > 0) {
				String temp1 = arrayRute[i - 1];
				String temp2 = arrayRute[i];
				if (temp1.equals("toko") && temp2.equals("tempat 1")) {
					tempTotalJarak += jarak[0];
				} else if (temp1.equals("tempat 1") && temp2.equals("tempat 2")) {
					tempTotalJarak += jarak[1];
				} else if (temp1.equals("tempat 2") && temp2.equals("tempat 3")) {
					tempTotalJarak += jarak[2];
				} else if (temp1.equals("tempat 3") && temp2.equals("tempat 4")) {
					tempTotalJarak += jarak[3];
				}
			}
		}

		int bensin = tempTotalJarak * 2 / 2500;

		System.out.println("Jumlah Bensin : " + bensin + " Liter");


	}

}

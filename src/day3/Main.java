package day3;

import java.util.Scanner;

public class Main {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan Angka Yang Anda Mau :  ");
		int n = input.nextInt();
		for (int i = 0; i < n; i++) {
			System.out.print(i + 1 + " ");
		}
		System.out.println(" ");
		System.out.println("===================");
		for (int i = n; i > 0; i--) {
			System.out.print(i + " ");
		}
		System.out.println(" ");
		input.close();
	}

}

package day3;

import java.util.Scanner;

public class Number4 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan Nilai N Anda : ");
		int n = input.nextInt();
		int j = 1;
		for (int i = 1; i <= n; i++) {
			System.out.print(j + " ");
			j = j + 4;
		}
		input.close();
	}

}

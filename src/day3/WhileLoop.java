package day3;
import java.util.Scanner;
public class WhileLoop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan Jumlah Perulangan Anda : ");
		int n = input.nextInt();
		int i = 0;
		while (i < n ) {
			System.out.print(i + 1 + " ");
			i++;
		}
		System.out.print(" || ");
		int j = n; //set input sebagai pembatas
		while (j > 0) {
			System.out.print(j + " ");
			j--;
		}
		input.close();//memberhentikan perintah Scanner
	}

}

package day3;

import java.util.Scanner;

public class Number2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan Nilai N Anda : ");
		int n = input.nextInt();
		for (int i = 1; i <= n; i++) {
			System.out.print((i * 2) + " ");
		}
		input.close();
	}

}

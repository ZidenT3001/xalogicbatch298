package day3;

import java.util.Scanner;

public class DoWhile {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan Nilai N : ");
		int n = input.nextInt();
		int i = 0;
		 do {
			 if (i == (n - 1)) {
				 System.out.print(i + 1);
			 }
			 else {
				 System.out.print((i + 1) + ",");
			 }			 
			 i++;
		 } while (i < n );
		 input.close();

	}

}

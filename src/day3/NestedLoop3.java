package day3;

import java.util.Scanner;

public class NestedLoop3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan nilai max perulangan : ");
		int n = input.nextInt();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <  n; j++) {
				if (i + j >= n - 1) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println(" ");
		}
		input.close();

	}

}

package day3;

import java.util.Scanner;

public class Number5 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan Nilai N Anda : ");
		int n = input.nextInt();
		int j = 1;
		for (int i = 0; i < n; i++) {
			if (i / 2 == 0) {
				System.out.print("*"); // * 
				j+=4;
			}
			else {
				System.out.print(j + " ");
				j+=4;
			}
		}
		input.close();
		
	}

}

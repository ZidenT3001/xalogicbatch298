package tugasOop;

import java.util.Scanner;

import inheritance.lingkaran;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// atribut
		Scanner input = new Scanner(System.in);
		System.out.print("Inputkan nama Pelajar anda ");
		String nama = input.nextLine();
		Pelajar pelajar = new Pelajar();
		pelajar.nama = nama;
		pelajar.stress = 10;
		Fisika fisika = new Fisika();
		Kimia kimia = new Kimia();
		Biologi biologi = new Biologi();
		Bindo bindo = new Bindo();
		Bing bing = new Bing();
		Istirahat istirahat = new Istirahat();
		int str = 10;
		matematika Matematika = new matematika();
		System.out.print("apakah anda berangkat sekolah ");
		String ans = input.next();
		while (ans.equals("y")) {
			if (str < 65) {
				System.out.print("mapel anda ");
				String mapel = input.next().toLowerCase();
				if (mapel.equals("fisika")) {
					System.out.print("berpa lama mapel ini ");
					int jam = input.nextInt();
					fisika.jam = jam;
					fisika.belajar();
					str += fisika.tingkatStress;

				}
				if (mapel.equals("matematika")) {
					System.out.print("berpa lama mapel ini ");
					int jam = input.nextInt();
					Matematika.jam = jam;
					Matematika.belajar();
					str += Matematika.tingkatStress;

				}
				if (mapel.equals("kimia")) {
					System.out.print("berpa lama mapel ini ");
					int jam = input.nextInt();
					kimia.jam = jam;
					kimia.belajar();
					str += kimia.tingkatStress;

				}
				if (mapel.equals("biologi")) {
					System.out.print("berpa lama mapel ini ");
					int jam = input.nextInt();
					biologi.jam = jam;
					biologi.belajar();
					str += biologi.tingkatStress;

				}
				if (mapel.equals("bindo")) {
					System.out.print("berpa lama mapel ini ");
					int jam = input.nextInt();
					bindo.jam = jam;
					bindo.belajar();
					str += bindo.tingkatStress;

				}
				if (mapel.equals("bing")) {
					System.out.print("berpa lama mapel ini ");
					int jam = input.nextInt();
					bing.jam = jam;
					bing.belajar();
					str += bing.tingkatStress;

				}

			} else {
				System.out.println("stress anda tinggi ! anda mengambil istirahat !");
				istirahat.istirahat();
				str = str - istirahat.tingkatStress;

			}
			System.out.print("lanjut? ");
			input.nextLine();
			String ans2 = input.nextLine();
			ans = ans2;
		}
		System.out.println("nama : " + pelajar.nama);
		System.out.println("tingkat stress anda : " + str);
	}

}

package inheritance;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//membuat object bangun datar
		bangundatar bangunDatar = new bangundatar();
		
		//membuat object persegi dan mengisi nilai properti
		persegi Persegi = new persegi();
		Persegi.sisi = 2;
		
		//membuat object lingkaran dan mengisi r
		lingkaran Lingkaran = new lingkaran();
		Lingkaran.r = 22;
		
		persegiPanjang PersegiPanjang = new persegiPanjang();
		PersegiPanjang.panjang = 20;
		PersegiPanjang.lebar = 10;
		
		segitiga Segitiga = new segitiga();
        Segitiga.alas = 12;
        Segitiga.tinggi = 8;
        
        // memanggila method luas dan keliling
        bangunDatar.luas();
        bangunDatar.keliling();
        
        Persegi.luas();
        Persegi.keliling();
        
        Lingkaran.luas();
        Lingkaran.keliling();
        
        PersegiPanjang.luas();
        PersegiPanjang.keliling();
        
        Segitiga.luas();
        Segitiga.keliling();
	}

}

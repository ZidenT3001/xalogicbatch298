package day6;

import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter Nomor yang anda : ");
				int number = input.nextInt();
				input.nextLine();
				switch (number) {
					case 1:
						soal1();
						break;
					case 2:
						soal2();
						break;
					case 3:
						soal3();
						break;
					case 4:
						soal4();
						break;
					case 5:
						soal5();
						break;
					case 6:
						soal6();
						break;
					case 7:
						soal7();
						break;
					default :
						System.out.println("Input Anda Salah !");
						break;
				
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println();
			System.out.println("continue?");
			answer = input.next();
		
	}

}
	
	private static void soal1() {

		System.out.println("Inputkan Nilai Fubonacci annda : ");
		int n = input.nextInt(); 
        long[] series = new long[n];
        series[0] = 1;
        series[1] = 1;
        for(int i=2; i < n; i++)
        {
            series[i] = series[i-1] + series[i-2];
        }
        System.out.println("Fibonacci Series upto " + n);
                
        for(int i=0; i< n; i++)
        {
            System.out.print(series[i] + " ");
        }
    }
	
	private static void soal2() {
		System.out.println("Inputkan Nilai Fubonacci annda : ");
		int n = input.nextInt();     
        long[] series = new long[n];
        series[0] = 1;
        series[1] = 1;
        series[2] = 1;
        for(int i=3; i < n; i++)
        {
            series[i] = series[i-1] + series[i-2] + series[i-3];
        }
        System.out.println("Fibonacci Series upto " + n);
        for(int i=0; i< n; i++)
        {
            System.out.print(series[i] + " ");
        }
	}

	private static void soal3() {
		System.out.println("Inputkan panjang array anda : ");
		int n = input.nextInt();
		for (int i = 2; i < n; i++) {
			int count = 0;
			for (int j = 1; j <= i; j++) {
				if (i%j==0) {
					count = count + 1;
				}
			}
			if (count == 2) {
				if (i == n -1) {
					System.out.print(i);
				}else {
					System.out.print(i + ",");
				}
				
			}
		}
	}
	private static void soal4() {
		System.out.println("Inputkan waktu saat ini dengan format jj:mm:dd:pm/am : ");
		String n = input.nextLine().toUpperCase(); //07:08:45PM
		String[] array = n.split(":"); //array[] = [07,08,45PM]
		for (int i = 0; i < array.length; i++) {
			char[] arrayChar = array[i].toCharArray(); //arrayChar[0] : [0,7]
			for ( int j = 0; j < arrayChar.length; j++) { // arrayChar[length-1] = [4,5,P,M]
				if (arrayChar[j] == 'P') { 
					if ((Integer.parseInt(array[0])) > 12){
						array[array.length-1] = array[array.length-1].substring(0, 2);
					}else {
					array[0]=(Integer.parseInt(array[0]) + 12) +""; // array[0] > int > +12 > string > array[0]
					array[array.length-1] = array[array.length-1].substring(0, 2);
					}
				}
				if (Integer.parseInt(array[0]) > 12) {
					array[array.length-1] = array[array.length-1].substring(0, 2);
				}						
				else{
					array[0]=((Integer.parseInt(array[0]) - (Integer.parseInt(array[0]))) + "0");
					array[array.length-1] = array[array.length-1].substring(0, 2);
				}	
			}
		}
		for (int k = 0; k < array.length; k++) { 
			if (k == array.length-1) {
				System.out.print(array[k]);
			}else {
				System.out.print(array[k]  + ":");
			}			
		}
	}
	private static void soal5() {
		Scanner a = new Scanner(System. in );
        System.out.println("Masukkan angka");
        int nilai = a.nextInt();
        System.out.println("Faktor-faktornya:");
        int prima = 2;
        int result = 0;
        while(nilai !=1 ) {
        	result = nilai;
        	if (result%prima==0) {
        		result=nilai/prima;
        		System.out.println("" + nilai + "/" + prima + "=" + result);
        		nilai = result;
            }
        	else {
        		prima++; 
        	}        	       	
        }       
    }
		
	private static void soal6() {
		System.out.println("Inputkan rute anda : ");
		String rute = input.nextLine();
		int konsumen1 = 2000, konsumen2 = 500, konsumen3 = 1500, konsumen4 = 300;
		String[] rute1 = rute.split(" ");
		String jarak = "";		
		int jarakTempuh = 0;
		for (int i = 0; i < rute1.length; i++) {
			if ( Integer.parseInt(rute1[i]) == 1) {
				jarakTempuh = jarakTempuh + konsumen1;
				jarak = jarak + ((konsumen1) + "");
			}
			else if ( Integer.parseInt(rute1[i]) == 2) {
				jarakTempuh = jarakTempuh + konsumen2;
				jarak = jarak + ((konsumen2) + "");
			}
			else if ( Integer.parseInt(rute1[i]) == 3) {
				jarakTempuh = jarakTempuh + konsumen3;
				jarak = jarak + ((konsumen3) + "");
			}
			else if ( Integer.parseInt(rute1[i]) == 4) {
				jarakTempuh = jarakTempuh + konsumen4;
				jarak = jarak + ((konsumen4) + "") + " + ";
			}
		}
		System.out.println("Jarak Tempuh : " + jarak + " = " + (jarakTempuh/1000) + " KM");
		System.out.println("BBM Yang Digunakan = " + (jarakTempuh/1000)/2 + " Liter");	
	}
	
	private static void soal7() {
		System.out.println("Inputkan Pesan Anda : ");
		String pesan = input.nextLine().toLowerCase();
		char[] signal = pesan.toCharArray();
		int temp = 0;
		for (int i = 0; i < signal.length; i+=3) {
			if (signal[i] != 's') {
				temp++;
			}
			if ( signal[i+1] != 'o') {
				temp++;
			}
			if ( signal[i+2] != 's') {
				temp++;
			}				
			
		}
		System.out.println(temp);
		
	}
	}
		

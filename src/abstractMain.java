
public class abstractMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// membuat objek dari class Triangle
		Shape segitiga = new Triangle(4, 5);

		// membuat objek dari class Circle
		Shape lingkaran = new Circle(10);

		System.out.println("Luas Segitiga: " + segitiga.getArea());
		System.out.println("Luas Lingkaran: " + lingkaran.getArea());
	}

}

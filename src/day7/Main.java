package day7;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				String answer = "Y";
				while (answer.toUpperCase().equals("Y")) {
					try {
						System.out.print("Enter Nomor yang anda : ");
						int number = input.nextInt();
						input.nextLine();
						switch (number) {
							case 1:
								soal1();
								break;
							case 2:
								soal2();
								break;
							case 3:
								soal3();
								break;
							case 4:
								soal4();
								break;
							case 5:
								soal5();
								break;
							case 6:
								soal6();
								break;
							case 7:
								soal7();
								break;
							case 8:
								soal8();
								break;
							case 9:
								soal9();
								break;
							case 10:
								soal10();
								break;
							default :
								System.out.println("Input Anda Salah !");
								break;
						
						}
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
					
					System.out.println();
					System.out.println("continue?");
					answer = input.next();
				}

	}
	
	private static void soal1() {
		System.out.print("Inputkan Nilai pertam : ");
		int a = input.nextInt();
		System.out.print("Inputkan Nilai pertam : ");
		int b = input.nextInt();
		System.out.print("Hasil : " + (a + b));
    }
	
	private static void soal2() {
		System.out.println("Inputkan waktu saat ini dengan format jj:mm:dd:pm/am : ");
		String n = input.nextLine().toUpperCase(); //07:08:45PM
		String[] array = n.split(":"); //array[] = [07,08,45PM]
		for (int i = 0; i < array.length; i++) {
			char[] arrayChar = array[i].toCharArray(); //arrayChar[0] : [0,7]
			for ( int j = 0; j < arrayChar.length; j++) { // arrayChar[length-1] = [4,5,P,M]
				if (arrayChar[j] == 'P') { 
					if ((Integer.parseInt(array[0])) > 12){
						array[array.length-1] = array[array.length-1].substring(0, 2);
					}else {
					array[0]=(Integer.parseInt(array[0]) + 12) +""; // array[0] > int > +12 > string > array[0]
					array[array.length-1] = array[array.length-1].substring(0, 2);
					}
				}
				if (Integer.parseInt(array[0]) > 12) {
					array[array.length-1] = array[array.length-1].substring(0, 2);
				}						
				else{
					array[0]=((Integer.parseInt(array[0]) - (Integer.parseInt(array[0]))) + "0");
					array[array.length-1] = array[array.length-1].substring(0, 2);
				}	
			}
		}
		for (int k = 0; k < array.length; k++) { 
			if (k == array.length-1) {
				System.out.print(array[k]);
			}else {
				System.out.print(array[k]  + ":");
			}			
		}
	}

	private static void soal3() {
		System.out.println("Inputkan Array Anda : ");
		String array = input.nextLine();
		String[] array1 = array.split(" ");
		int result = 0;
		for (int i = 0; i < array1.length; i++) {
			result = result + Integer.parseInt(array1[i]);
		}
		System.out.println(result);
	}
	private static void soal4() {
		System.out.print("Inputkan nilai n untuk matrix (nxn) : ");
		int n = input.nextInt();
		int [][] matrix = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print("inputkan value array ke - " + i + " " + j + " : ");
				int value = input.nextInt();
				matrix[i][j] = value;
			}
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println("");
		}
		System.out.println("");
		int primer = 0;
		int sekunder = 0;
		int diagonalDifferce = 0;
		for (int i = 0; i < n; i++)
	    {
	        for (int j = 0; j < n; j++)
	        {
	            if (i == j)
	                primer += matrix[i][j];
	            if (i == n - j - 1)
	                sekunder += matrix[i][j];
	        }
	    }
		System.out.println("");
		System.out.println("primer : " + (primer));
		System.out.println("sekunder : " + (sekunder));
		diagonalDifferce = Math.abs(primer) - Math.abs(sekunder);
		diagonalDifferce = Math.abs(diagonalDifferce);
		System.out.println("diference : " + (diagonalDifferce));		
	}
	private static void soal5() {
		System.out.println("Inputkan panjang array anda : ");
		String n = input.nextLine();
		String[] arr = n.split(" ");
		DecimalFormat dF = new DecimalFormat("0.000000");
		float positif = 0, negatif = 0, zero = 0;
		float hasil1 = 0, hasil2 =0, hasil3 = 0;
		int result = 0;
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		for (int i = 0; i < arr.length; i++) {
			if (Integer.parseInt(arr[i])%2==0) {
				positif = positif + 1; 
			}
			if (Integer.parseInt(arr[i])<0) {
				negatif = positif + 1; 
			}
			if (Integer.parseInt(arr[i])==0) {
				zero = zero + 1; 
			}
		}
			
		
		hasil1 = positif / arr.length;
		hasil2 = negatif / arr.length;
		hasil3 = zero / arr.length;
		System.out.println("");
		System.out.println("Hasil Positif : " + dF.format(hasil1));
		System.out.println("Hasil Negatif : " + dF.format(hasil2));
		System.out.println("Hasil Nol : " + dF.format(hasil3));
		
		}
		
    
		
	private static void soal6() {
		System.out.print("Inputkan nilai n untuk matrix (nxn) : ");
		int n = input.nextInt();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <  n; j++) {
				if (i + j >= n - 1) {
					System.out.print("#");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println(" ");
		}
		}
		
	
	private static void soal7() {		
        long min = Long.MAX_VALUE;
        long max = 0;
        long sum = 0;
        for(int i=0; i<5; i++)
        {	
        	System.out.println("Inputkan Value array anda : ");
            long curr = input.nextLong();  
            if(max < curr)
            {
                max = curr;
            }
            if(min > curr)
            {
                min = curr;
            }
            
            sum += curr;
        }
        long minSum = sum - max;
        long maxSum = sum - min;
        System.out.println(minSum + " " + maxSum);
	}
	private static void soal8() {
		System.out.println("Inputkan panjang array anda : ");
        int n = input.nextInt();
        int max = 0;
        int sum = 0;
        int num;
        for(int i =0; i < n; i++){
        	System.out.println("Inputkan nilai array anda : ");
            num = input.nextInt();
            if(num > max){
                sum = 1;
                max = num;
            }else if(num == max){
                sum++;
            }
        }
        System.out.println(sum);
        			
	}
	private static void soal9() {
		System.out.println("Inputkan panjang array anda : ");
		String n = input.nextLine();
		String[] arr = n.split(" ");
		long result = 0;
		for (int i = 0; i < arr.length; i++) {
			result = result + Integer.parseInt(arr[i]);
			//System.out.println(result);
			//System.out.println(result);
		}
		System.out.println("output : " + result);			
	}
	private static void soal10() {
		System.out.println("Inputkan panjang array anda : ");
		int n = input.nextInt();
		int[] listA = new int[n];
		int[] listB = new int[n];
		for (int a = 0; a < n; a++) {
			System.out.println("inputkan index array ke" + a);
			listA[a] = input.nextInt();
		}
		for (int b = 0; b < n; b++) {
			System.out.println("inputkan index array ke" + b);
			listB[b] = input.nextInt();
		}
		int valueA = 0;
		int valueB = 0;
		for (int x = 0; x < n; x++) {
			if (listA[x] > listB[x]) {
				valueA++;
			}
			else if (listB[x] > listA[x]) {
				valueB++;
			}
		}
		System.out.println(valueA + " " + valueB);
			
		}
			
	
}

package day8;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.print("Enter Nomor yang anda : ");
				int number = input.nextInt();
				input.nextLine();
				switch (number) {
					case 1:
						soal1();
						break;
					case 2:
						soal2();
						break;
					case 3:
						soal3();
						break;
					case 4:
						soal4();
						break;
					case 5:
						soal5();
						break;
					case 6:
						soal6();
						break;
					case 7:
						soal7();
						break;
					case 8:
						soal8();
						break;
					case 9:
						soal9();
						break;
					case 10:
						soal10();
						break;
					default :
						System.out.println("Input Anda Salah !");
						break;
				
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println();
			System.out.println("continue?");
			answer = input.next();
		}

}

private static void soal1() {
	System.out.println("Inputkan kata anda : ");
	String n = input.nextLine();
	char[] arr = n.toCharArray();
	int count = 1;
	for (int i = 0; i < arr.length; i++) {
		if (Character.isUpperCase(arr[i])) {
			count+=1;
		}
	}
	System.out.println(count);	
}

private static void soal2() {
	System.out.println("Inputkan kata anda : ");
	String n = input.nextLine();
	char[] arr = n.toCharArray();
	int digit = 0;
	int countNum = 0;
	int countUp = 0;
	int countLow = 0;
	int countSp = 0;
	char[] number = {'0','1','2','3','4','5','6','7','8','9'};
	char[] lowCase = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	char[] uperCase = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','p','Q','R','S','T','U','V','W','X','Y','Z'};
	char[] specialChar = {'!','@','#','$','%','^','&','*','(',')','-','+'};
	if (arr.length < 6) {;
		digit = 6 - arr.length;
		System.out.println(digit);
	}
	else {
		//mencari number
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < number.length; j++) {
				if (arr[i] == number[j]) {
					countNum++;					
				}
			}
		}
		//mencari upper case
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < uperCase.length; j++) {
				if (arr[i] == uperCase[j]) {
					countUp++;					
				}
			}
		}
		//mencari lower case
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < lowCase.length; j++) {
				if (arr[i] == lowCase[j]) {
					countLow++;					
				}
			}
		}
		//mencari special Case
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < specialChar.length; j++) {
				if (arr[i] == specialChar[j]) {
					countSp++;					
				}
			}
		}		
	}
	
	System.out.println(countNum);
	System.out.println(countUp);
	System.out.println(countLow);
	System.out.println(countSp);
	int kategori = 4;
	if (countNum > 0) {
		kategori--;
		if (countUp > 0) {
			kategori--;
			if (countLow > 0 ) {
				kategori--;
				if (countSp > 0 ) {
					kategori--;					
				}
			}
		}
	}
	System.out.println(kategori);
}

private static void soal3() {
	System.out.print("Inputkan array anda : ");
	String inputan = input.nextLine().toLowerCase();
	System.out.print("Inputkan Jumlah Rotation anda : ");
	int rotation = input.nextInt();
	char[] arrayInputan = inputan.toCharArray();	
	char[] lowCase = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	int[] index = new int[arrayInputan.length];
	// Mnampilkan nilai Array yang di inputan
	for (int i = 0; i < arrayInputan.length; i++) {
		System.out.print(arrayInputan[i]);
	}
	System.out.println();
	// abcdefghijklmnopqrstuvwxyz
	// alif
	// cdefghijklmnopqrstuvwxyzab
	for (int i = 0; i < arrayInputan.length; i++) { // 0
		for (int j = 0; j < lowCase.length; j++) { 
			// a a 
			if (arrayInputan[i] == lowCase[j]) { // arr[0] = a == low[0] = c
				index[i]=j;
				//System.out.print(index[i]);
			}
		}
	}
	// a,l,i,f
	for (int i = 0; i < rotation; i++) {
		char first = lowCase[0]; // a > 
		for (int j = 0; j < lowCase.length-1; j++) { 
			lowCase[j] = lowCase[j+1]; // l,i,f
		}
		lowCase[lowCase.length-1]=first; // l,i,f,a
	}
	System.out.println("");
	System.out.println("Array Acak: ");
	for ( int k = 0; k < lowCase.length; k++) {
		System.out.print(lowCase[k]);
	}
	System.out.println();
	for (int a = 0; a < index.length; a++) {
		for ( int b = 0; b < lowCase.length; b++) {
			if (index[a] == b) {
				System.out.print(lowCase[b]);
			}
		}
	}	
	System.out.println();
}
private static void soal4() {
	System.out.println("Inputkan Pesan Anda : ");
	String pesan = input.nextLine().toLowerCase();
	char[] signal = pesan.toCharArray();
	int temp = 0;
	for (int i = 0; i < signal.length; i+=3) {
		if (signal[i] != 's') {
			temp++;
		}
		if ( signal[i+1] != 'o') {
			temp++;
		}
		if ( signal[i+2] != 's') {
			temp++;
		}		
		
	}
	System.out.println(temp);
}
private static void soal5() {
	System.out.print("Inputkan array anda : ");
	String inputan = input.nextLine().toLowerCase();
	char[] arrIn = inputan.toCharArray();
	System.out.print("Inputkan array kedua anda : ");
	String inputan2 = input.nextLine().toLowerCase();
	char[] arrIn2 = inputan2.toCharArray();
	char[] pembanding = {'h','a','c','k','e','r','r','a','n','k'};  
	int count = 0;
	int count2 = 0;
	//untuk array 1
	for (int i = 0; i < arrIn.length; i++) {
		if (count != pembanding.length && arrIn[i] == pembanding[count]) {
			count++;
		}
	}
	// untuk array 2
	for (int i = 0; i < arrIn2.length; i++) {
		if (count2 != pembanding.length && arrIn2[i] == pembanding[count2]) {
			count2++;
		}
	}
	//System.out.println(count);
	//untuk array 1
	if (count == 10) {
		System.out.println("YES");
	}
	else {
		System.out.println("NO");
	}
	//untuk array 2
	if (count2 == 10) {
		System.out.println("YES");
	}
	else {
		System.out.println("NO");
	}
}

private static void soal6() {
	System.out.print("Inputkan array anda : ");
	String inputan = input.nextLine().toLowerCase();
	char[] arrIn = inputan.toCharArray();
	char[] lowCase = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		// w,s,d
		int count = 0;
		for (int a = 0; a < lowCase.length; a++) { //w
			for (int b = 0; b < arrIn.length; b++) { // a
				if (arrIn[b] == lowCase[a]) { // w/s == a/b.....w = 25 x //: false
					count++;
					break;
				}
			}
		}
	//System.out.println(count);
	if (count == 26) {
		System.out.println("Pangram");
	}
	else {
		System.out.println("Not Pangram");
	}
}

private static void soal7() {
    long a = 0;
    long first = 0;
    boolean result = false;
    	System.out.print("Inputkan array anda : ");
        String in = input.next();
        for (int j = 1; j < in.length()+1 && j < 18; j++) {
            a = Long.parseLong(in.substring(0, j));        
            String temp = "" + a;
            int count = 1;
            while (temp.length() < in.length()) {
                a++;
                count++;
                temp += a;
            }
            if (temp.equals(in) && count >= 2) {               
                result = true;
                first = Long.parseLong(in.substring(0, j));
            }         
        }
        if (result == true ) {
        	System.out.println("YES " + first);
        }
        else {
        	System.out.println("NO");
        }    
}
private static void soal8() {
	System.out.println("Inputkan kata pertama : ");
	String a = input.nextLine();
	char[] arr1 = a.toCharArray();
	int result = 0;	
	System.out.println("Inputkan kata keuda : ");
	String b = input.nextLine();
	char[] arr2 = b.toCharArray();
	System.out.println("Inputkan kata keuda : ");
	String c = input.nextLine();
	char[] arr3 = c.toCharArray();
	int count = 0;
	for (int i = 0; i < arr1.length; i++) {
		for (int j = 0; j < arr2.length;j++) {
			for (int k = 0; k < arr3.length; k++) {
				if (arr1[i]== arr2[j]) {
					if (arr2[j] == arr3[k]) {
						count+=1;
						break;
					}
				}
			}
		}
	}
	System.out.println("To make anagram we must delete " + count + " Character");
}
private static void soal9() {
	System.out.println("Inputkan kata pertama : ");
	String a = input.nextLine();
	char[] arr1 = a.toCharArray();
	int result = 0;	
	System.out.println("Inputkan kata keuda : ");
	String b = input.nextLine();
	char[] arr2 = b.toCharArray();
	int count = 0;
	for (int i = 0; i < arr1.length; i++) {
		for (int j = 0; j < arr2.length;j++) {
			if (arr1[i] == arr2[j]) { //c == b
				count += 1;
				break;
			}
		}
	}
	result = (arr1.length + arr2.length)- (count*2);
	System.out.println("To make anagram we must delete " + result + " Character");
	
	}
private static void soal10() {
	System.out.print("Inputkan array anda : ");
	String inputan = input.nextLine().toLowerCase();
	char[] arrIn = inputan.toCharArray();
	System.out.print("Inputkan array kedua anda : ");
	String inputan2 = input.nextLine().toLowerCase();
	char[] arrIn2 = inputan2.toCharArray();
	System.out.print("Inputkan array ke 3 anda : ");
	String inputan3 = input.nextLine().toLowerCase();
	char[] arrIn3 = inputan3.toCharArray();
	System.out.print("Inputkan array ke 4 anda : ");
	String inputan4 = input.nextLine().toLowerCase();
	char[] arrIn4 = inputan4.toCharArray();
	// Proses pertama
	int count = 0;
	for (int i = 0; i < arrIn.length; i++) {
		for ( int j = 0; j < arrIn2.length; j++) {
			if (arrIn[i] == arrIn2[j]) {
				count++;
			}
		}
	}
	if (count > 1) {
		System.out.println("YES");		
	}
	else {
		System.out.println("NO");
	}
	//Proses kedua
	int count2 = 0;
	for (int i = 0; i < arrIn3.length; i++) {
		for ( int j = 0; j < arrIn4.length; j++) {
			if (arrIn[i] == arrIn2[j]) {
				count2++;
			}
		}
	}
	if (count2 > 1) {
		System.out.println("YES");		
	}
	else {
		System.out.println("NO");
	}


}

}
